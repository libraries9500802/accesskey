package com.creelayer.accessKey.core.cases;

import com.creelayer.accessKey.core.command.ValidateAccessKeyCommand;
import com.creelayer.accessKey.core.common.type.Scopes;
import com.creelayer.accessKey.core.common.type.Secret;
import com.creelayer.accessKey.core.exception.AccessKeyValidationException;
import com.creelayer.accessKey.core.exception.EntityNotFoundException;
import com.creelayer.accessKey.core.model.AccessKey;
import com.creelayer.accessKey.core.outgoing.AccessKeyRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RemoveAccessKeyTest {

    @Mock
    private AccessKeyRepository accessKeyRepository;

    @InjectMocks
    private RemoveAccessKey removeAccessKey;

    @Test
    void whenRemoveKey_thenAllPassed() {

        UUID uuid = UUID.randomUUID();
        Secret secret = new Secret("test secret");
        Scopes scopes = new Scopes(new String[]{"scope1", "scope2", "scope3", "scope4"});
        AccessKey key = new AccessKey(uuid, null, secret, null, scopes, LocalDateTime.now(), LocalDateTime.now());

        when(accessKeyRepository.findById(uuid)).thenAnswer(i -> Optional.of(key));

        removeAccessKey.handle(uuid);

        verify(accessKeyRepository, times(1)).findById(uuid);
        verify(accessKeyRepository, times(1)).delete(key);
    }


    @Test
    void whenNotExistKey_thenExceptionExpected() {

        UUID uuid = UUID.randomUUID();

        when(accessKeyRepository.findById(uuid)).thenAnswer(i -> Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> removeAccessKey.handle(uuid));
        verify(accessKeyRepository, only()).findById(uuid);
    }

}