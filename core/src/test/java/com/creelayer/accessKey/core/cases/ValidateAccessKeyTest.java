package com.creelayer.accessKey.core.cases;

import com.creelayer.accessKey.core.command.ValidateAccessKeyCommand;
import com.creelayer.accessKey.core.common.type.Scopes;
import com.creelayer.accessKey.core.common.type.Secret;
import com.creelayer.accessKey.core.exception.AccessKeyValidationException;
import com.creelayer.accessKey.core.model.AccessKey;
import com.creelayer.accessKey.core.outgoing.AccessKeyRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(org.mockito.junit.jupiter.MockitoExtension.class)
class ValidateAccessKeyTest {


    @Mock
    private AccessKeyRepository accessKeyRepository;

    @InjectMocks
    private ValidateAccessKey validateAccessKey;

    @Test
    void whenAcceptableKey_thenValidatePassed() {

        UUID uuid = UUID.randomUUID();
        Secret secret = new Secret("test secret");
        Scopes scopes = new Scopes(new String[]{"scope1", "scope2"});
        AccessKey key = new AccessKey(uuid, null, secret, null, scopes, LocalDateTime.now(), LocalDateTime.now());
        ValidateAccessKeyCommand command = new ValidateAccessKeyCommand(uuid, secret, scopes);

        when(accessKeyRepository.findById(uuid)).thenAnswer(i -> Optional.of(key));

        validateAccessKey.handle(command);

        verify(accessKeyRepository, only()).findById(uuid);
    }

    @Test
    void whenInvalidSecret_thenExceptionExpected() {

        UUID uuid = UUID.randomUUID();
        Secret secret = new Secret("test secret");
        Secret invalidSecret = new Secret("test secret1");
        Scopes scopes = new Scopes(new String[]{"scope1", "scope2", "scope3", "scope4"});
        AccessKey key = new AccessKey(uuid, null, secret, null, scopes, LocalDateTime.now(), LocalDateTime.now());
        ValidateAccessKeyCommand command = new ValidateAccessKeyCommand(uuid, invalidSecret, scopes);

        when(accessKeyRepository.findById(uuid)).thenAnswer(i -> Optional.of(key));

        assertThrows(AccessKeyValidationException.class, () -> validateAccessKey.handle(command));
        verify(accessKeyRepository, only()).findById(uuid);
    }

    @Test
    void whenValidPartialScopes_thenValidatePassed() {

        UUID uuid = UUID.randomUUID();
        Secret secret = new Secret("test secret");
        Scopes scopes = new Scopes(new String[]{"scope1", "scope3", "scope2", "scope4"});
        Scopes needScopes = new Scopes(new String[]{"scope1", "scope3"});
        AccessKey key = new AccessKey(uuid, null, secret, null, scopes, LocalDateTime.now(), LocalDateTime.now());
        ValidateAccessKeyCommand command = new ValidateAccessKeyCommand(uuid, secret, needScopes);

        when(accessKeyRepository.findById(uuid)).thenAnswer(i -> Optional.of(key));

        validateAccessKey.handle(command);

        verify(accessKeyRepository, only()).findById(uuid);
    }

    @Test
    void whenInvalidScopes_thenExceptionExpected() {

        UUID uuid = UUID.randomUUID();
        Secret secret = new Secret("test secret");
        Scopes scopes = new Scopes(new String[]{"scope1", "scope2", "scope4"});
        Scopes needScopes = new Scopes(new String[]{"scope1", "scope3"});
        AccessKey key = new AccessKey(uuid, null, secret, null, scopes, LocalDateTime.now(), LocalDateTime.now());
        ValidateAccessKeyCommand command = new ValidateAccessKeyCommand(uuid, secret, needScopes);

        when(accessKeyRepository.findById(uuid)).thenAnswer(i -> Optional.of(key));

        assertThrows(AccessKeyValidationException.class, () -> validateAccessKey.handle(command));
        verify(accessKeyRepository, only()).findById(uuid);
    }

    @Test
    void whenNotExistKey_thenExceptionExpected() {

        UUID uuid = UUID.randomUUID();
        Secret secret = new Secret("test secret");
        Scopes scopes = new Scopes(new String[]{"scope1", "scope2"});
        ValidateAccessKeyCommand command = new ValidateAccessKeyCommand(uuid, secret, scopes);

        when(accessKeyRepository.findById(uuid)).thenAnswer(i -> Optional.empty());

        assertThrows(AccessKeyValidationException.class, () -> validateAccessKey.handle(command));
        verify(accessKeyRepository, only()).findById(uuid);
    }
}