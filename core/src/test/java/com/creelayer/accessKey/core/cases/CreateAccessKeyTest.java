package com.creelayer.accessKey.core.cases;

import static org.mockito.Mockito.*;

import org.mockito.junit.jupiter.MockitoExtension;
import com.creelayer.accessKey.core.command.CreateAccessKeyCommand;
import com.creelayer.accessKey.core.common.Identity;
import com.creelayer.accessKey.core.common.type.AccessKeyDescription;
import com.creelayer.accessKey.core.common.type.Scopes;
import com.creelayer.accessKey.core.model.AccessKey;
import com.creelayer.accessKey.core.outgoing.AccessKeyRepository;
import com.creelayer.accessKey.core.outgoing.RandomStringGenerator;
import com.creelayer.accessKey.core.projection.OneTimeAccessKey;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class CreateAccessKeyTest {


    @Mock
    private AccessKeyRepository accessKeyRepository;

    @Mock
    private RandomStringGenerator randomStringGenerator;

    @InjectMocks
    private CreateAccessKey createAccessKey;

    @Test
    void whenCreateKey_thenOneTimeKeyReturn() {

        when(accessKeyRepository.save(any(AccessKey.class))).thenAnswer(i -> i.getArguments()[0]);
        when(randomStringGenerator.generate()).thenReturn("testSecret");


        Identity identity = UUID::randomUUID;
        AccessKeyDescription description = new AccessKeyDescription("Test desc");
        Scopes scopes = new Scopes(new String[]{"scope1", "scope2"});
        CreateAccessKeyCommand command = new CreateAccessKeyCommand(identity, description, scopes);
        OneTimeAccessKey key = createAccessKey.handle(command);

        verify(accessKeyRepository, only()).save(any(AccessKey.class));
        verify(randomStringGenerator, only()).generate();

        assertNotNull(key.uuid());
        assertEquals(key.secret(),"testSecret");
        assertNotNull(key.description());

    }
}