package com.creelayer.accessKey.core.model;

import com.creelayer.accessKey.core.common.Identity;
import com.creelayer.accessKey.core.common.type.AccessKeyDescription;
import com.creelayer.accessKey.core.common.type.Scopes;
import com.creelayer.accessKey.core.common.type.Secret;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

public class AccessKey {


    @Getter
    private final UUID uuid;

    @Getter
    private final Identity holder;

    @Getter
    private final Secret secret;

    @Getter
    @Setter
    private AccessKeyDescription description;

    @Getter
    @Setter
    private Scopes scopes;

    @Getter
    private final LocalDateTime createdAt;

    @Getter
    @Setter
    private LocalDateTime updatedAt;


    public AccessKey(Identity holder, Secret secret) {

        Objects.requireNonNull(holder, "Holder is null");
        Objects.requireNonNull(secret, "Secret is null");

        this.uuid = UUID.randomUUID();
        this.holder = holder;
        this.secret = secret;
        this.createdAt = this.updatedAt = LocalDateTime.now();
    }

    public AccessKey(Identity holder, Secret secret, AccessKeyDescription description) {
        this(holder, secret);
        this.description = description;
    }

    public AccessKey(Identity holder, Secret secret, AccessKeyDescription description, Scopes scopes) {
        this(holder, secret, description);
        this.scopes = scopes;
    }

    public AccessKey(UUID uuid, Identity holder, Secret secret, AccessKeyDescription description, Scopes scopes, LocalDateTime createdAt, LocalDateTime updatedAt) {
        Objects.requireNonNull(uuid, "Uuid is null");
        this.uuid = uuid;
        this.holder = holder;
        this.secret = secret;
        this.description = description;
        this.scopes = scopes;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }


    public boolean acceptable(EntryPass entryPass) {
        return uuid.equals(entryPass.uuid())
                && secret.equals(entryPass.secret())
                && scopes.allMatch(entryPass.scopes());
    }
}
