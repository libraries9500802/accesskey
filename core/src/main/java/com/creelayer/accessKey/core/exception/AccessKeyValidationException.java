package com.creelayer.accessKey.core.exception;

public class AccessKeyValidationException extends BaseException {
    public AccessKeyValidationException() {
    }

    public AccessKeyValidationException(String message) {
        super(message);
    }
}
