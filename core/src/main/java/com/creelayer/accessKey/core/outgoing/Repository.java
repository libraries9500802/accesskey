package com.creelayer.accessKey.core.outgoing;


import java.util.Optional;

public interface Repository<T, U> {

    Optional<T> findById(U id);

    T save(T entity);

    void deleteById(U id);

    void delete(T entity);

}
