package com.creelayer.accessKey.core.outgoing;

public interface RandomStringGenerator {

    int DEFAULT_RANDOM_LENGTH = 40;

    default String generate() {
        return generate(DEFAULT_RANDOM_LENGTH);
    }

    String generate(Integer length);
}
