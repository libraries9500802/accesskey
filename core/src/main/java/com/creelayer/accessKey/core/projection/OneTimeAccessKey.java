package com.creelayer.accessKey.core.projection;

import com.creelayer.accessKey.core.model.AccessKey;

import java.util.UUID;

public record OneTimeAccessKey(UUID uuid, String secret, String description, String[] scopes) {

    public OneTimeAccessKey(AccessKey key) {
        this(key.getUuid(), key.getSecret().toString(), key.getDescription().toString(), key.getScopes().items());
    }
}
