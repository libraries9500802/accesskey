package com.creelayer.accessKey.core.outgoing;

import com.creelayer.accessKey.core.model.AccessKey;

import java.util.UUID;

public interface AccessKeyRepository extends Repository<AccessKey, UUID> {

}
