package com.creelayer.accessKey.core.cases;

import com.creelayer.accessKey.core.exception.EntityNotFoundException;
import com.creelayer.accessKey.core.incoming.AccessKeyRemoveHandler;
import com.creelayer.accessKey.core.model.AccessKey;
import com.creelayer.accessKey.core.outgoing.AccessKeyRepository;

import java.util.UUID;

public class RemoveAccessKey implements AccessKeyRemoveHandler {

    private final AccessKeyRepository repository;

    public RemoveAccessKey(AccessKeyRepository repository) {
        this.repository = repository;
    }

    @Override
    public void handle(UUID uuid) {
        AccessKey key = repository.findById(uuid)
                .orElseThrow(() -> new EntityNotFoundException("Access key not found"));
        repository.delete(key);
    }
}
