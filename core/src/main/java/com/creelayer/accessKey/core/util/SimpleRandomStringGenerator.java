package com.creelayer.accessKey.core.util;

import com.creelayer.accessKey.core.outgoing.RandomStringGenerator;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Random;

@NoArgsConstructor
@AllArgsConstructor
public class SimpleRandomStringGenerator implements RandomStringGenerator {

    private String SALTCHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    @Override
    public String generate(Integer length) {
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < length) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();
    }
}
