package com.creelayer.accessKey.core.model;

import com.creelayer.accessKey.core.common.type.Scopes;
import com.creelayer.accessKey.core.common.type.Secret;

import java.util.UUID;

public record EntryPass(UUID uuid, Secret secret, Scopes scopes) {
}
