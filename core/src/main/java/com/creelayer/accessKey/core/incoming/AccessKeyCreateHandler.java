package com.creelayer.accessKey.core.incoming;

import com.creelayer.accessKey.core.command.CreateAccessKeyCommand;
import com.creelayer.accessKey.core.common.handler.SupplierCommandHandler;
import com.creelayer.accessKey.core.projection.OneTimeAccessKey;

public interface AccessKeyCreateHandler extends SupplierCommandHandler<CreateAccessKeyCommand, OneTimeAccessKey> {

}
