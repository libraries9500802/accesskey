package com.creelayer.accessKey.core.common.handler;

public interface CommandHandler<T> {

     void handle(T command);

}
