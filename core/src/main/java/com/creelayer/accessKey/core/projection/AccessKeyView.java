package com.creelayer.accessKey.core.projection;

import java.util.UUID;

public record AccessKeyView(UUID uuid, String description, String[] scopes) {
}
