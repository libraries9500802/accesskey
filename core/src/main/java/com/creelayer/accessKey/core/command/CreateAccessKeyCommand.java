package com.creelayer.accessKey.core.command;

import com.creelayer.accessKey.core.common.Identity;
import com.creelayer.accessKey.core.common.type.AccessKeyDescription;
import com.creelayer.accessKey.core.common.type.Scopes;

public record CreateAccessKeyCommand(Identity holder, AccessKeyDescription description, Scopes scopes) {
}
