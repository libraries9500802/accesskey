package com.creelayer.accessKey.core.common;

import java.security.Principal;

public interface AccessKeyHolderIdentityFactory {
    Identity identity(Principal authentication);
}
