package com.creelayer.accessKey.core.common.type;

import java.util.Objects;

public record Secret(String value) {

    private final static int MIN_SECRET_LENGTH = 10;

    private final static int MAX_SECRET_LENGTH = 255;


    public Secret {

        Objects.requireNonNull(value, "Secret value is required");

        if (value.length() < MIN_SECRET_LENGTH)
            throw new IllegalArgumentException(String.format("Min secret length is %s chars", MIN_SECRET_LENGTH));

        if (value.length() > MAX_SECRET_LENGTH)
            throw new IllegalArgumentException(String.format("Max secret length is %s chars", MAX_SECRET_LENGTH));
    }

    @Override
    public String toString() {
        return value;
    }
}
