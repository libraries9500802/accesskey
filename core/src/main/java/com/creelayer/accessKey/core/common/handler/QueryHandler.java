package com.creelayer.accessKey.core.common.handler;

public interface QueryHandler<I, O> {
    O ask(I input);
}
