package com.creelayer.accessKey.core.command;

import com.creelayer.accessKey.core.common.type.Scopes;
import com.creelayer.accessKey.core.common.type.Secret;
import java.util.UUID;

public record ValidateAccessKeyCommand(UUID uuid, Secret secret, Scopes scopes) {
}
