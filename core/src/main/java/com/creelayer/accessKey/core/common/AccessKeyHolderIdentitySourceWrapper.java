package com.creelayer.accessKey.core.common;


public interface AccessKeyHolderIdentitySourceWrapper {
    Identity wrap(Object uid);
}
