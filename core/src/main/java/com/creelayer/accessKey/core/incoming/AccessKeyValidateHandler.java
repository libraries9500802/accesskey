package com.creelayer.accessKey.core.incoming;

import com.creelayer.accessKey.core.command.ValidateAccessKeyCommand;
import com.creelayer.accessKey.core.common.handler.CommandHandler;

public interface AccessKeyValidateHandler extends CommandHandler<ValidateAccessKeyCommand[]> {

}
