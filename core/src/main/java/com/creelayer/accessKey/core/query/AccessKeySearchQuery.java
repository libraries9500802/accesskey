package com.creelayer.accessKey.core.query;

import com.creelayer.accessKey.core.common.Identity;

public record AccessKeySearchQuery(Identity holder, String search) {

    private final static int MAX_SEARCH_LENGTH = 100;

    public AccessKeySearchQuery {
        if (search != null && search.length() > MAX_SEARCH_LENGTH)
            throw new IllegalArgumentException(String.format("Max search length is %s chars", MAX_SEARCH_LENGTH));
    }
}
