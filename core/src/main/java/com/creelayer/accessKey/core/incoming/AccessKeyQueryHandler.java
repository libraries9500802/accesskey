package com.creelayer.accessKey.core.incoming;

import com.creelayer.accessKey.core.projection.AccessKeyView;
import com.creelayer.accessKey.core.query.AccessKeySearchQuery;

import java.util.List;
import java.util.UUID;

public interface AccessKeyQueryHandler {

    AccessKeyView getByUuId(UUID uuid);

    List<AccessKeyView> search(AccessKeySearchQuery query);

}
