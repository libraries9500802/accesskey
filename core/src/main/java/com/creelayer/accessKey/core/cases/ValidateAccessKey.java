package com.creelayer.accessKey.core.cases;

import com.creelayer.accessKey.core.command.ValidateAccessKeyCommand;
import com.creelayer.accessKey.core.exception.AccessKeyValidationException;
import com.creelayer.accessKey.core.incoming.AccessKeyValidateHandler;
import com.creelayer.accessKey.core.model.AccessKey;
import com.creelayer.accessKey.core.model.EntryPass;
import com.creelayer.accessKey.core.outgoing.AccessKeyRepository;

public class ValidateAccessKey implements AccessKeyValidateHandler {

    private final AccessKeyRepository repository;

    public ValidateAccessKey(AccessKeyRepository repository) {
        this.repository = repository;
    }

    @Override
    public void handle(ValidateAccessKeyCommand... commands) {

        for (ValidateAccessKeyCommand command : commands) {

            AccessKey key = repository.findById(command.uuid())
                    .orElseThrow(() -> new AccessKeyValidationException("Invalid access key"));

            EntryPass entryPass = new EntryPass(command.uuid(), command.secret(), command.scopes());

            if (!key.acceptable(entryPass))
                throw new AccessKeyValidationException("Invalid access key");
        }
    }
}
