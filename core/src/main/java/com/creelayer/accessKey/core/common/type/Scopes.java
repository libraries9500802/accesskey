package com.creelayer.accessKey.core.common.type;


import java.util.Arrays;

public record Scopes(String[] items) {

    private final static int MAX_SCOPE_LENGTH = 50;

    private final static int MAX_DESCRIPTION_LENGTH = 200;

    public Scopes {
        if (items != null)
            for (String scope : items)
                if (scope.length() > MAX_SCOPE_LENGTH)
                    throw new IllegalArgumentException(String.format("Max scope length is %s chars", MAX_DESCRIPTION_LENGTH));
    }

    public boolean allMatch(Scopes scopes) {
        return Arrays.stream(scopes.items).allMatch(v1 -> Arrays.asList(items).contains(v1));
    }

}
