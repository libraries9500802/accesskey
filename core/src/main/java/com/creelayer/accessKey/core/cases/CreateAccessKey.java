package com.creelayer.accessKey.core.cases;

import com.creelayer.accessKey.core.command.CreateAccessKeyCommand;
import com.creelayer.accessKey.core.common.type.Secret;
import com.creelayer.accessKey.core.incoming.AccessKeyCreateHandler;
import com.creelayer.accessKey.core.model.AccessKey;
import com.creelayer.accessKey.core.outgoing.AccessKeyRepository;
import com.creelayer.accessKey.core.outgoing.RandomStringGenerator;
import com.creelayer.accessKey.core.projection.OneTimeAccessKey;

public class CreateAccessKey implements AccessKeyCreateHandler {

    private final RandomStringGenerator random;

    private final AccessKeyRepository repository;

    public CreateAccessKey(RandomStringGenerator random, AccessKeyRepository repository) {
        this.random = random;
        this.repository = repository;
    }

    @Override
    public OneTimeAccessKey handle(CreateAccessKeyCommand command) {
        AccessKey key = new AccessKey(command.holder(), new Secret(random.generate()), command.description(), command.scopes());
        return new OneTimeAccessKey(repository.save(key));
    }
}
