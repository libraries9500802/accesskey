package com.creelayer.accessKey.core.common.handler;

public interface SupplierCommandHandler<T, R>  {
    R handle(T command);
}
