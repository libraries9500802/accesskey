package com.creelayer.accessKey.core.common.type;


public record AccessKeyDescription(String value) {

    private final static int MAX_DESCRIPTION_LENGTH = 200;

    public AccessKeyDescription {
        if (value.length() > MAX_DESCRIPTION_LENGTH)
            throw new IllegalArgumentException(String.format("Max description length is %s chars", MAX_DESCRIPTION_LENGTH));
    }

    @Override
    public String toString() {
        return value;
    }
}
