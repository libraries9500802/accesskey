package com.creelayer.accessKey.core.exception;

public class EntityNotFoundException extends BaseException {
    public EntityNotFoundException() {
    }

    public EntityNotFoundException(String message) {
        super(message);
    }
}
