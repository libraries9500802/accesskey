package com.creelayer.accessKey.core.incoming;

import com.creelayer.accessKey.core.common.handler.CommandHandler;

import java.util.UUID;

public interface AccessKeyRemoveHandler extends CommandHandler<UUID> {

}
