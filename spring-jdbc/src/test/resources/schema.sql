create table IF NOT EXISTS access_key
(
    uuid        uuid                    not null primary key,
    holder      uuid,
    secret      varchar(255)            not null,
    description varchar(255),
    scopes      character varying[],
    created_at  timestamp default now() not null,
    updated_at  timestamp default now() not null
);
