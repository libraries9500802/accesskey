package com.creelayer.accessKey.jdbc.spring.persistence;

import com.creelayer.accessKey.core.model.AccessKey;
import com.creelayer.accessKey.core.outgoing.AccessKeyRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Sql({"/sql/clear.sql","/sql/access.sql"})
class JdbcAccessKeyQueryHandlerTest {

    @Autowired
    public AccessKeyRepository repository;

    @Test
    void getByUuId() {
        AccessKey key = repository.findById(UUID.fromString("847aa6b5-55ce-4001-adac-01378d680838"))
                .orElse(null);
        assertNotNull(key);
    }
}