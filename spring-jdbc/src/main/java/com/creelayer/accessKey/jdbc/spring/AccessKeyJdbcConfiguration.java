package com.creelayer.accessKey.jdbc.spring;


import com.creelayer.accessKey.core.cases.CreateAccessKey;
import com.creelayer.accessKey.core.cases.RemoveAccessKey;
import com.creelayer.accessKey.core.cases.ValidateAccessKey;
import com.creelayer.accessKey.core.common.AccessKeyHolderIdentityFactory;
import com.creelayer.accessKey.core.common.AccessKeyHolderIdentitySourceWrapper;
import com.creelayer.accessKey.core.common.Identity;
import com.creelayer.accessKey.core.incoming.AccessKeyCreateHandler;
import com.creelayer.accessKey.core.incoming.AccessKeyQueryHandler;
import com.creelayer.accessKey.core.incoming.AccessKeyRemoveHandler;
import com.creelayer.accessKey.core.incoming.AccessKeyValidateHandler;
import com.creelayer.accessKey.core.outgoing.AccessKeyRepository;
import com.creelayer.accessKey.core.util.SimpleRandomStringGenerator;
import com.creelayer.accessKey.jdbc.spring.persistence.JdbcAccessKeyQueryHandler;
import com.creelayer.accessKey.jdbc.spring.persistence.JdbcAccessKeyRepository;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.UUID;

@Configuration
public class AccessKeyJdbcConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public AccessKeyHolderIdentityFactory accessKeyHolderIdentityFactory() {
        return authentication -> (Identity) () -> UUID.fromString(authentication.getName());
    }

    @Bean
    @ConditionalOnMissingBean
    public AccessKeyHolderIdentitySourceWrapper accessKeyHolderIdentitySourceWrapper() {
        return uid -> () -> UUID.fromString((String) uid);
    }

    @Bean
    @ConditionalOnMissingBean
    public AccessKeyRepository accessKeyRepository(
            JdbcTemplate jdbcTemplate,
            AccessKeyHolderIdentitySourceWrapper holderWrapper
    ) {
        return new JdbcAccessKeyRepository(jdbcTemplate, holderWrapper);
    }

    @Bean
    @ConditionalOnMissingBean
    public AccessKeyQueryHandler accessKeyQueryHandler(NamedParameterJdbcTemplate jdbcTemplate) {
        return new JdbcAccessKeyQueryHandler(jdbcTemplate);
    }

    @Bean
    @ConditionalOnMissingBean
    public AccessKeyCreateHandler accessKeyCreateHandler(AccessKeyRepository accessKeyRepository) {
        return new CreateAccessKey(new SimpleRandomStringGenerator(), accessKeyRepository);
    }

    @Bean
    @ConditionalOnMissingBean
    public AccessKeyValidateHandler accessKeyValidateHandler(AccessKeyRepository accessKeyRepository) {
        return new ValidateAccessKey(accessKeyRepository);
    }

    @Bean
    @ConditionalOnMissingBean
    public AccessKeyRemoveHandler accessKeyRemoveHandler(AccessKeyRepository accessKeyRepository) {
        return new RemoveAccessKey(accessKeyRepository);
    }

}
