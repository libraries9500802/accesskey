package com.creelayer.accessKey.jdbc.spring.persistence;


import com.creelayer.accessKey.core.common.AccessKeyHolderIdentitySourceWrapper;
import com.creelayer.accessKey.core.common.type.AccessKeyDescription;
import com.creelayer.accessKey.core.common.type.Scopes;
import com.creelayer.accessKey.core.common.type.Secret;
import com.creelayer.accessKey.core.exception.EntityNotFoundException;
import com.creelayer.accessKey.core.model.AccessKey;
import com.creelayer.accessKey.core.outgoing.AccessKeyRepository;

import lombok.Setter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.*;

public class JdbcAccessKeyRepository implements AccessKeyRepository {

    private final JdbcTemplate jdbcTemplate;

    private final AccessKeyHolderIdentitySourceWrapper holderWrapper;

    @Setter
    private String table = "access_key";

    @Setter
    private String SQL_SELECT_BY_ID = "select * from %s where uuid = ?";

    @Setter
    private String SQL_DELETE_BY_ID = "DELETE FROM %s WHERE uuid = ?";


    public JdbcAccessKeyRepository(JdbcTemplate jdbcTemplate, AccessKeyHolderIdentitySourceWrapper holderWrapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.holderWrapper = holderWrapper;
    }

    @Override
    public Optional<AccessKey> findById(UUID uuid) {
        AccessKey accessKey = jdbcTemplate
                .queryForObject(
                        String.format(SQL_SELECT_BY_ID, table),
                        new AccessKeyRowMapper(this.holderWrapper),
                        uuid
                );

        return Optional.ofNullable(accessKey);
    }

    @Override
    public AccessKey save(AccessKey accessKey) {

        accessKey.setUpdatedAt(LocalDateTime.now());

        Map<String, Object> data = new HashMap<>();
        data.put("uuid", accessKey.getUuid());
        data.put("holder", accessKey.getHolder().getUid());
        data.put("secret", accessKey.getSecret());
        data.put("description", accessKey.getDescription());
        data.put("scopes", accessKey.getScopes() != null ? accessKey.getScopes().items(): null);
        data.put("created_at", accessKey.getCreatedAt());
        data.put("updated_at", accessKey.getUpdatedAt());

        new SimpleJdbcInsert(Objects.requireNonNull(jdbcTemplate.getDataSource()))
                .withTableName(table).execute(data);

        return accessKey;
    }

    @Override
    public void deleteById(UUID uuid) {
        int result = jdbcTemplate.update(String.format(SQL_DELETE_BY_ID, table), uuid);

        if (result == 0)
            throw new EntityNotFoundException("Access key not found");
    }

    @Override
    public void delete(AccessKey accessKey) {
        int result = jdbcTemplate.update(String.format(SQL_DELETE_BY_ID, table), accessKey.getUuid());

        if (result == 0)
            throw new EntityNotFoundException("Access key not found");
    }

    private static class AccessKeyRowMapper implements RowMapper<AccessKey> {

        private final AccessKeyHolderIdentitySourceWrapper holderWrapper;

        public AccessKeyRowMapper(AccessKeyHolderIdentitySourceWrapper holderWrapper) {
            this.holderWrapper = holderWrapper;
        }

        @Override
        public AccessKey mapRow(ResultSet rs, int rowNum) throws SQLException {

            return new AccessKey(
                    UUID.fromString(rs.getString("uuid")),
                    holderWrapper.wrap(rs.getObject("holder")),
                    new Secret(rs.getString("secret")),
                    new AccessKeyDescription(rs.getString("description")),
                    new Scopes((String[]) rs.getArray("scopes").getArray()),
                    rs.getTimestamp("created_at").toLocalDateTime(),
                    rs.getTimestamp("updated_at").toLocalDateTime()
            );
        }
    }
}
