package com.creelayer.accessKey.jdbc.spring.persistence;



import com.creelayer.accessKey.core.exception.EntityNotFoundException;
import com.creelayer.accessKey.core.incoming.AccessKeyQueryHandler;
import com.creelayer.accessKey.core.projection.AccessKeyView;
import com.creelayer.accessKey.core.query.AccessKeySearchQuery;
import lombok.Setter;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;


public class JdbcAccessKeyQueryHandler implements AccessKeyQueryHandler {

    private final NamedParameterJdbcTemplate jdbc;

    @Setter
    private String table = "access_key";

    @Setter
    private String SQL_SELECT_BY_ID = "SELECT * FROM %s WHERE uuid = ?";

    @Setter
    private String SQL_SELECT_ALL_BY_HOLDER = "SELECT * FROM %s WHERE holder = :holder";

    public JdbcAccessKeyQueryHandler(NamedParameterJdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public AccessKeyView getByUuId(UUID uuid) {
        try {
            return jdbc
                    .getJdbcTemplate()
                    .queryForObject(String.format(SQL_SELECT_BY_ID, table), new AccessKeyViewMapper(), uuid);
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException("Access key not found");
        }
    }

    @Override
    public List<AccessKeyView> search(AccessKeySearchQuery query) {
        SqlParameterSource parameters = new MapSqlParameterSource("holder", query.holder().getUid());
        return jdbc.query(String.format(SQL_SELECT_ALL_BY_HOLDER, table), parameters, new AccessKeyViewMapper());
    }

    private static class AccessKeyViewMapper implements RowMapper<AccessKeyView> {
        @Override
        public AccessKeyView mapRow(ResultSet rs, int rowNum) throws SQLException {

            String[] scopes = rs.getArray("scopes") != null ?
                    (String[]) rs.getArray("scopes").getArray() : null;

            return new AccessKeyView(
                    UUID.fromString(rs.getString("uuid")),
                    rs.getString("description"),
                    scopes
            );
        }
    }
}
